.defcolor lightblue #92b4f3
.defcolor red #d05b5b

.\" bullet list formatting
.de BL
.IP \(bu 2
..

.\" example formating
.de EX
.br
.gcolor red
.BI Example : 
.br
.de eg end
.	gcolor red
	\\\\$1
.	br
.	gcolor
.end
.gcolor
..

.\" shell command
.de cm
.br
.PP
.gcolor blue
$ \\$1
.gcolor
.br
..

.\" code listing
.de code
.fam C
.ad 0
.nf
.in +0.5i
..
.de /code
.fam T
.ad b
.fi
.in
..
