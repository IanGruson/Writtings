.if !dPS .ds PS
.if !dPE .ds PE
.lf 1 git_cheatsheet.ms
.so ../macros.ms
.TL
Git Cheatsheet
.AU
Meedos

.SH
Introduction 
.LP
This is a short cheatsheet on how I use Git daily. I don't use the more complex
commands yet as I haven't learned them. Hope this is useful to some of you.
I'll try to break it up in use-cases or scenarios with examples as I believe it
is easier to understand. It is highly suggested that you consult the man pages
for every command that you try as it is impossible to cover all the flags in
this cheatsheet.

.SH 
Initializing a new git repo
.LP
On an existing codebase/project or on a empty directory type : 
.cm "git init"
This command creates a hidden folder named .git where all the git magic happens
(hashes, working dir, etc...).
.br
By default this creates the default branch "master" which you are on now, more
on that later.
.SH 
Pulling a repository
.LP
If your see a repository that you wish to get a copy of on github.com or gitlab.com
you can do so by cloning it with the command : 
.cm "git clone https://gitlab.com/iangruson/writtings.git
If you wish to use http or : 
.cm "git clone git@gitlab.com:iangruson/writtings.git
if you want to use ssh.
With this you won't have to initialize the git repo like in the previous step
because it is already done.
.SH 
Most basic usage 
.LP
The following commands are the most used commands by far, you'll do them a
couple of times a day. 
.SH 2
Add work to the index
.cm "git add ."
This command adds your files to the index waiting to be committed. The "." means
all that is within the current directory ("*" works fine too). As an image, "git
add" takes a snapshot of the content of the working tree, that snapshot will
then be used for the next commit. 
.SH 3
Committing
To save your changes to the repository 
.cm "git commit -m 'This is a commit message' " 
This creates a new commit with the current index's content. 

.\" fig1
.lf 57
.PS 1.700i 2.250i
.\" -0.375 -0.75 1.875 0.95
.\" 0.000i 1.700i 2.250i 0.000i
.nr 00 \n(.u
.nf
.nr 0x 1
\h'2.250i'
.sp -1
.fcolor lightblue
\h'0.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\D't -1.000p'\h'1.000p'
.sp -1
\h'0.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 58
\h'0.375i-(\w'C0'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C0
.sp -1
.gcolor lightblue
.lf 59
\h'0.375i-(\w'master'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master
.sp -1
.gcolor
.fcolor lightblue
\h'1.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'1.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 60
\h'1.375i-(\w'C1'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C1
.sp -1
\h'0.625i'\v'0.950i'\D'l 0.500i 0.000i'
.sp -1
.lf 62
\h'1.875i-(\w'HEAD*'u/2u)'\v'0.250i-(0v/2u)+0v+0.22m'HEAD*
.sp -1
\&\D'Fg 0.000'
.sp -1
\h'1.375i'\v'0.700i'\D'P 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't 0.100p'\h'-0.100p'
.sp -1
\h'1.375i'\v'0.700i'\D'p 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't -1.000p'\h'1.000p'
.sp -1
\h'1.875i'\v'0.500i'\D'l -0.407i 0.163i'
.sp -1
.sp 1.700i+1
.if \n(00 .fi
.br
.nr 0x 0
.lf 64
.PE
.lf 65

HEAD* is where you are currently in the tree. Doing another commit will continue the tree
to the right with "C2" so on and so forth.

.SH 2
Pushing to a remote
.PP
Let's say you are working with a team and the code base is meant to be stored on
a remote server such as gitlab.com, gitea, or even github.com. You first need to
create a repository on one of those services and push you local changes to that
remote. The next commands will assume you already created a repo at
https://gitlab.com/meedos/test_repo.git
.SH 3 
Adding a remote
.PP
You can add a remote with a given name and a url with. 
.cm "git remote add origin https://gitlab.com/meedos/test_repo.git"
A local repo can have multiple remotes.
You can then proceed to push ("send") your changes to the repo with the adequate
name : 
.cm "git push origin master"
Also you can set a default upstream for a branch that way you don't have to type
the whole command every time.
.cm "git push --set-upstream origin master
This way you can simply type : 
.cm "git push"
To push to origin/master.
.SH 2
Pulling a remote
.PP
Now, let's say a coworker pushes work to you remote repository, or you simply
want to access you work from a different computer, you'll want to pull that work
onto your local repository. To achieve this use : 
.cm "git pull origin master"
Or if you've setup the upstream, simply : 
.cm "git pull"

.UL "Note : " this command fetches the work and merges it on your branch. In some
cases it is preferable to use "git fetch" instead to avoid automatic merging.

If you are extremely lucky and work alone, you could use only these commands and
get away with it. But that won't happen and there's is so much more useful
aspects to git.
.SH 2
Branching 
.PP 
Branching as it names implies allows you to create a diverging branch to your
git tree. Branching should become second nature as it is a good practice to
implement a feature on a separate branch without messing out your master branch.
Some create a branch for each versions, for each big functionality (or User
Story), or even a branch per platform (Linux, Windows) in some cases.
.br
In any case, to create a branch type (and choose a branch name) : 
.cm "git branch [BRANCH_NAME]"
Let's say we named our branch Sprint-1. The result can be mentally seen as : 
.\" fig2
.lf 121
.PS 1.900i 2.250i
.\" -0.375 -0.95 1.875 0.95
.\" 0.000i 1.900i 2.250i 0.000i
.nr 00 \n(.u
.nf
.nr 0x 1
\h'2.250i'
.sp -1
.fcolor lightblue
\h'0.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'0.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 122
\h'0.375i-(\w'C0'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C0
.sp -1
.gcolor lightblue
.lf 123
\h'0.375i-(\w'master'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master
.sp -1
.gcolor
.fcolor lightblue
\h'1.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'1.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 124
\h'1.375i-(\w'C1'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C1
.sp -1
.gcolor lightblue
.lf 125
\h'1.375i-(\w'master*'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master*
.sp -1
.gcolor
.gcolor lightblue
.lf 126
\h'1.375i-(\w'Sprint-1'u/2u)'\v'1.650i-(0v/2u)+0v+0.22m'Sprint-1
.sp -1
.gcolor
\h'0.625i'\v'0.950i'\D'l 0.500i 0.000i'
.sp -1
.lf 128
\h'1.875i-(\w'HEAD*'u/2u)'\v'0.250i-(0v/2u)+0v+0.22m'HEAD*
.sp -1
\&\D'Fg 0.000'
.sp -1
\h'1.375i'\v'0.700i'\D'P 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't 0.100p'\h'-0.100p'
.sp -1
\h'1.375i'\v'0.700i'\D'p 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't -1.000p'\h'1.000p'
.sp -1
\h'1.875i'\v'0.500i'\D'l -0.407i 0.163i'
.sp -1
.sp 1.900i+1
.if \n(00 .fi
.br
.nr 0x 0
.lf 130
.PE
.lf 131
The new branch is up to date from the branch it was created from. 
.SH 2 
Checking out
.PP
Now is a good time to talk about "git checkout". This command allows you to move
around the tree. You can switch branches or go to previous commits
.br
.UL "switch branch : "
.cm "git checkout Sprint-1"
.UL "Go to commit C0 :"
.cm "git checkout C0"
result : 
.\" fig3
.lf 144
.PS 1.900i 1.750i
.\" -0.375 -0.95 1.375 0.95
.\" 0.000i 1.900i 1.750i 0.000i
.nr 00 \n(.u
.nf
.nr 0x 1
\h'1.750i'
.sp -1
.fcolor lightblue
\h'0.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'0.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 145
\h'0.375i-(\w'C0'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C0
.sp -1
.gcolor lightblue
.lf 146
\h'0.375i-(\w'master'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master
.sp -1
.gcolor
.fcolor lightblue
\h'1.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'1.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 147
\h'1.375i-(\w'C1'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C1
.sp -1
.gcolor lightblue
.lf 148
\h'1.375i-(\w'master*'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master*
.sp -1
.gcolor
.gcolor lightblue
.lf 149
\h'1.375i-(\w'Sprint-1'u/2u)'\v'1.650i-(0v/2u)+0v+0.22m'Sprint-1
.sp -1
.gcolor
\h'0.625i'\v'0.950i'\D'l 0.500i 0.000i'
.sp -1
.lf 151
\h'0.875i-(\w'HEAD*'u/2u)'\v'0.250i-(0v/2u)+0v+0.22m'HEAD*
.sp -1
\&\D'Fg 0.000'
.sp -1
\h'0.375i'\v'0.700i'\D'P 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't 0.100p'\h'-0.100p'
.sp -1
\h'0.375i'\v'0.700i'\D'p 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't -1.000p'\h'1.000p'
.sp -1
\h'0.875i'\v'0.500i'\D'l -0.407i 0.163i'
.sp -1
.sp 1.900i+1
.if \n(00 .fi
.br
.nr 0x 0
.lf 153
.PE
.lf 154
.\" fig4
Now let's say you are on the commit C1 on the branch Sprint-1 and you decide to
commit.
The result will look like this : 
.lf 158
.PS 1.700i 3.250i
.\" -0.375 -0.75 2.875 0.95
.\" 0.000i 1.700i 3.250i 0.000i
.nr 00 \n(.u
.nf
.nr 0x 1
\h'3.250i'
.sp -1
.fcolor lightblue
\h'0.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'0.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 159
\h'0.375i-(\w'C0'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C0
.sp -1
.gcolor lightblue
.lf 160
\h'0.375i-(\w'master'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master
.sp -1
.gcolor
.fcolor lightblue
\h'1.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'1.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 161
\h'1.375i-(\w'C1'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C1
.sp -1
.gcolor lightblue
.lf 162
\h'1.375i-(\w'master*'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master*
.sp -1
.gcolor
.fcolor red
\h'2.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'2.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 163
\h'2.375i-(\w'C2'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C2
.sp -1
.gcolor lightblue
.lf 164
\h'2.375i-(\w'Sprint-1'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'Sprint-1
.sp -1
.gcolor
\h'1.625i'\v'0.950i'\D'l 0.500i 0.000i'
.sp -1
\h'2.125i'\v'0.950i'\D'l 0.000i 0.000i'
.sp -1
\h'0.625i'\v'0.950i'\D'l 0.500i 0.000i'
.sp -1
.lf 167
\h'2.875i-(\w'HEAD*'u/2u)'\v'0.250i-(0v/2u)+0v+0.22m'HEAD*
.sp -1
\&\D'Fg 0.000'
.sp -1
\h'2.375i'\v'0.700i'\D'P 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't 0.100p'\h'-0.100p'
.sp -1
\h'2.375i'\v'0.700i'\D'p 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't -1.000p'\h'1.000p'
.sp -1
\h'2.875i'\v'0.500i'\D'l -0.407i 0.163i'
.sp -1
.sp 1.700i+1
.if \n(00 .fi
.br
.nr 0x 0
.lf 169
.PE
.lf 170
.\" fig5
.lf 171
.PS 2.700i 3.250i
.\" -0.375 -1.75 2.875 0.95
.\" 0.000i 2.700i 3.250i 0.000i
.nr 00 \n(.u
.nf
.nr 0x 1
\h'3.250i'
.sp -1
.fcolor lightblue
\h'0.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'0.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 172
\h'0.375i-(\w'C0'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C0
.sp -1
.gcolor lightblue
.lf 173
\h'0.375i-(\w'master'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master
.sp -1
.gcolor
.fcolor lightblue
\h'1.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'1.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 174
\h'1.375i-(\w'C1'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C1
.sp -1
.gcolor lightblue
.lf 175
\h'2.375i-(\w'master*'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master*
.sp -1
.gcolor
.fcolor red
\h'2.125i'\v'1.950i'\D'C 0.500i'
.sp -1
\h'2.125i'\v'1.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 176
\h'2.375i-(\w'C2'u/2u)'\v'1.950i-(0v/2u)+0v+0.22m'C2
.sp -1
.gcolor lightblue
.lf 177
\h'2.375i-(\w'Sprint-1'u/2u)'\v'2.450i-(0v/2u)+0v+0.22m'Sprint-1
.sp -1
.gcolor
.fcolor lightblue
\h'2.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'2.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 178
\h'2.375i-(\w'C3'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C3
.sp -1
\h'1.625i'\v'0.950i'\D'l 0.500i 0.000i'
.sp -1
.gcolor lightblue
.lf 180
\h'2.375i-(\w'Sprint-1'u/2u)'\v'2.450i-(0v/2u)+0v+0.22m'Sprint-1
.sp -1
.gcolor
\h'1.375i'\v'1.200i'\D'~ 0.000i 0.500i 0.750i 0.250i'
.sp -1
\h'0.625i'\v'0.950i'\D'l 0.500i 0.000i'
.sp -1
.lf 183
\h'2.875i-(\w'HEAD*'u/2u)'\v'0.250i-(0v/2u)+0v+0.22m'HEAD*
.sp -1
\&\D'Fg 0.000'
.sp -1
\h'2.375i'\v'0.700i'\D'P 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't 0.100p'\h'-0.100p'
.sp -1
\h'2.375i'\v'0.700i'\D'p 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't -1.000p'\h'1.000p'
.sp -1
\h'2.875i'\v'0.500i'\D'l -0.407i 0.163i'
.sp -1
.sp 2.700i+1
.if \n(00 .fi
.br
.nr 0x 0
.lf 185
.PE
.lf 186
Once you are happy with what you've done on the Sprint-1 branch and want to
integrate it to your master branch, you will wan't to do a "merge".
.SH 1
Merging
.PP
Merging allows you to integrate work from a branch to another. Merging makes a
new commit. To merge your Sprint-1 branch to master, you first need to be on the
branch "master" and type : 
.cm "git merge Sprint-1"
If all goes well the result should look like this. 
.lf 196
.PS 2.700i 4.250i
.\" -0.375 -1.75 3.875 0.95
.\" 0.000i 2.700i 4.250i 0.000i
.nr 00 \n(.u
.nf
.nr 0x 1
\h'4.250i'
.sp -1
.fcolor lightblue
\h'0.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'0.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 197
\h'0.375i-(\w'C0'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C0
.sp -1
.gcolor lightblue
.lf 198
\h'0.375i-(\w'master'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master
.sp -1
.gcolor
.fcolor lightblue
\h'1.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'1.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 199
\h'1.375i-(\w'C1'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C1
.sp -1
.gcolor lightblue
.lf 200
\h'2.375i-(\w'master*'u/2u)'\v'1.450i-(0v/2u)+0v+0.22m'master*
.sp -1
.gcolor
.fcolor red
\h'2.125i'\v'1.950i'\D'C 0.500i'
.sp -1
\h'2.125i'\v'1.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 201
\h'2.375i-(\w'C2'u/2u)'\v'1.950i-(0v/2u)+0v+0.22m'C2
.sp -1
.gcolor lightblue
.lf 202
\h'2.375i-(\w'Sprint-1'u/2u)'\v'2.450i-(0v/2u)+0v+0.22m'Sprint-1
.sp -1
.gcolor
.fcolor lightblue
\h'2.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'2.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 203
\h'2.375i-(\w'C3'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C3
.sp -1
\h'1.625i'\v'0.950i'\D'l 0.500i 0.000i'
.sp -1
.gcolor lightblue
.lf 205
\h'2.375i-(\w'Sprint-1'u/2u)'\v'2.450i-(0v/2u)+0v+0.22m'Sprint-1
.sp -1
.gcolor
\h'1.375i'\v'1.200i'\D'~ 0.000i 0.500i 0.750i 0.250i'
.sp -1
\h'0.625i'\v'0.950i'\D'l 0.500i 0.000i'
.sp -1
.fcolor lightblue
\h'3.125i'\v'0.950i'\D'C 0.500i'
.sp -1
\h'3.125i'\v'0.950i'\D'c 0.500i'
.sp -1
.fcolor
.lf 208
\h'3.375i-(\w'C4'u/2u)'\v'0.950i-(0v/2u)+0v+0.22m'C4
.sp -1
\h'2.625i'\v'1.950i'\D'~ 0.500i 0.000i 0.250i -0.750i'
.sp -1
\h'2.625i'\v'0.950i'\D'l 0.500i 0.000i'
.sp -1
.lf 211
\h'3.875i-(\w'HEAD*'u/2u)'\v'0.250i-(0v/2u)+0v+0.22m'HEAD*
.sp -1
\&\D'Fg 0.000'
.sp -1
\h'3.375i'\v'0.700i'\D'P 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't 0.100p'\h'-0.100p'
.sp -1
\h'3.375i'\v'0.700i'\D'p 0.084i -0.060i 0.019i 0.046i'
.sp -1
\D't -1.000p'\h'1.000p'
.sp -1
\h'3.875i'\v'0.500i'\D'l -0.407i 0.163i'
.sp -1
.sp 2.700i+1
.if \n(00 .fi
.br
.nr 0x 0
.lf 213
.PE
.lf 214
.SH 2
Solving conflicts
.LP
But, everything doesn't always go as planned and sometimes git is not smart
enough to know what should be kept and what needs to be dropped. These are
called conflicts, and can require you to solve them manually. To do this there
are several different tools at your disposal, such vimdiff(1, 2, or 3),
gvimdiff, bc, etc. I've only tried vimdiff1, vimdiff2, and vimdiff3, which uses
vim, so I can't really talk about other tools.  
.code
void hello_world() {
<<<<<<< HEAD
printf("Hello world\n");
=======
char * name = "Meedos";
printf("Top o' the morning to you {}", name);
>>>>>>> C2
}
.LP
Here we have an example of two different implementations of a hello_world
function in C. One comes from the current branch we're on (master), and the
other implementation comes the branch we're trying to merge from (Sprint-1).
The part of code between the <<<<<<< and ======= markers is the code represents
the part of code present on the current working directory, in this case the
master branch. The code between the ======= and the >>>>>>> markers is the code
that comes from the branch we are merging from (here Sprint-1).\br
To solve this conflict you must choose whether you want to keep your
implementation, or the implementation of the other branch, or a bit both in some
cases. No matter what you choose to pick, you must remove all conflict markers
from the file. Once you are done solving all conflicts in you project, you must
commit your changes.\br
Vimdiff in all of this checks for all files that need manual merging, and cycles
through them in a terminal user interface until you are finished. 
.SH 1 
\ .gitignore
.PP
Often times you will have files in your repository you don't want to add to the
index or push it to the remote. These files can be logs, environment files,
files containing passwords, high volume files etc... It is possible to ignore
these files by specifying them in a ".gitignore" file. You can specify files,
directories, and also use wildcards to discard multiple targets.
.EX
.eg "log"
.eg "passwd"
.eg "images/*.png"
.LP
If log is a directory, the whole directory and its content will be ignored and
won't be added next time you enter a git add command. Same goes for the
passwd file and all files ending with .png in the images directory.\br
It is possible to check if a file or directory is correctly being ignored by git
with the following command : 
.cm "git check-ignore passwd"
If the path is outputted to stdout then the target is correctly ignored. If not
then something went wrong.

.SH 1
Rebasing
.PP
Rebasing is extremely important and very useful if you often have to rewrite
your commits. It is also useful when you work with a team, since you might have
to rebase your local branch from the main remote branch. We will look at those
two scenarios.
.SH 2
Rewritting history
.PP
Given the following rust code : 

.code
#[derive(Debug)]
struct Person {
    first_name: String,
    last_name: String,
    postal_adddress: String,
    age: u8,
    ss_number: u32,
}

fn main() {

    let person: Person = Person {
		first_name: String::from("John"),
		last_name:String::from("Doe"),
		postal_adddress: String::from("95 Passage de Vaugirard"),
		age: 42,
		ss_number: 012345678};

    println!("Hello {:?}", person);
    set_ss_number(person, 364798543);
}

fn set_ss_number(mut person: Person, new_ss: u32) {
    person.ss_number = new_ss;
}

.PP
Let's say you've made a mistake in one of your previous commits (not your last
one) and you would like to rewrite that commit to keep a clean history for your
colleagues.

.PSPIC -L images/git-graph-1.ps 3

We wish to rewrite the commit where we add the social security number attribute
instead of adding a fix commit on top of our history.

To do this type :
.cm "git graph HEAD~2 -i"

.LP
You will then be prompted with a screen showing your two last commits, aswell as
commented help text below. 
The command we're interested in is the edit command which let's us modify our
commit. 

You should have the following:
.br
\m[blue]pick\m[] 535ff58 \m[darkgreen]add: function to set person's social security number\m[]
.br
\m[blue]pick\m[] 9965250 \m[darkgreen]add: person's postal address\m[]

From which we want to change to :
.br
\m[blue]edit\m[] 535ff58 \m[darkgreen]add: function to set person's social security number\m[]
.br
\m[blue]pick\m[] 9965250 \m[darkgreen]add: person's postal address\m[]
.br
Then save and exit
.br
.code
Proceed to make the following changes : 
diff --git a/src/main.rs b/src/main.rs
index d5a9567..497b9b5 100644
--- a/src/main.rs
+++ b/src/main.rs
@@ -3,7 +3,7 @@ struct Person {
     first_name: String,
     last_name: String,
     age: u8,
-    ss_number: u32,
+    ss_number: String,
 }

 fn main() {
@@ -11,7 +11,7 @@ fn main() {
     let person: Person = Person {first_name: String::from("John"),
	 last_name:String::from("Doe"),
     age: 42, ss_number: 012345678};
     println!("Hello {:?}", person);
-    set_ss_number(person, 364798543);
+    set_ss_number(person, String::from("364798543"));
 }

.LP
 Save your file and make your commit:
.cm "git commit --amend --no-edit"
And finally:
.cm "git rebase --continue"
You're done, you've successfully rewritten history.
