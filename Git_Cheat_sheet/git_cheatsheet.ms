.so ../macros.ms
.TL
Git Cheatsheet
.AU
Meedos

.SH
Introduction 
.LP
This is a short cheatsheet on how I use Git daily. I don't use the more complex
commands yet as I haven't learned them. Hope this is useful to some of you.
I'll try to break it up in use-cases or scenarios with examples as I believe it
is easier to understand. It is highly suggested that you consult the man pages
for every command that you try as it is impossible to cover all the flags in
this cheatsheet.

.SH 
Initializing a new git repo
.LP
On an existing codebase/project or on a empty directory type : 
.cm "git init"
This command creates a hidden folder named .git where all the git magic happens
(hashes, working dir, etc...).
.br
By default this creates the default branch "master" which you are on now, more
on that later.
.SH 
Pulling a repository
.LP
If your see a repository that you wish to get a copy of on github.com or gitlab.com
you can do so by cloning it with the command : 
.cm "git clone https://gitlab.com/iangruson/writtings.git
If you wish to use http or : 
.cm "git clone git@gitlab.com:iangruson/writtings.git
if you want to use ssh.
With this you won't have to initialize the git repo like in the previous step
because it is already done.
.SH 
Most basic usage 
.LP
The following commands are the most used commands by far, you'll do them a
couple of times a day. 
.SH 2
Add work to the index
.cm "git add ."
This command adds your files to the index waiting to be committed. The "." means
all that is within the current directory ("*" works fine too). As an image, "git
add" takes a snapshot of the content of the working tree, that snapshot will
then be used for the next commit. 
.SH 3
Committing
To save your changes to the repository 
.cm "git commit -m 'This is a commit message' " 
This creates a new commit with the current index's content. 

.\" fig1
.PS
C0 : circle "C0" at (0,0) shaded "lightblue"; 
box invisible "master" at (0,-0.5) outline "lightblue";
C1 : circle "C1" at (1,0) shaded "lightblue";
line from C0.e to C1.w;
HEAD : box invisible "HEAD*" at (1.5, 0.7);
arrow from HEAD.s to C1.n;
.PE

HEAD* is where you are currently in the tree. Doing another commit will continue the tree
to the right with "C2" so on and so forth.

.SH 2
Pushing to a remote
.PP
Let's say you are working with a team and the code base is meant to be stored on
a remote server such as gitlab.com, gitea, or even github.com. You first need to
create a repository on one of those services and push you local changes to that
remote. The next commands will assume you already created a repo at
https://gitlab.com/meedos/test_repo.git
.SH 3 
Adding a remote
.PP
You can add a remote with a given name and a url with. 
.cm "git remote add origin https://gitlab.com/meedos/test_repo.git"
A local repo can have multiple remotes.
You can then proceed to push ("send") your changes to the repo with the adequate
name : 
.cm "git push origin master"
Also you can set a default upstream for a branch that way you don't have to type
the whole command every time.
.cm "git push --set-upstream origin master
This way you can simply type : 
.cm "git push"
To push to origin/master.
.SH 2
Pulling a remote
.PP
Now, let's say a coworker pushes work to you remote repository, or you simply
want to access you work from a different computer, you'll want to pull that work
onto your local repository. To achieve this use : 
.cm "git pull origin master"
Or if you've setup the upstream, simply : 
.cm "git pull"

.UL "Note : " this command fetches the work and merges it on your branch. In some
cases it is preferable to use "git fetch" instead to avoid automatic merging.

If you are extremely lucky and work alone, you could use only these commands and
get away with it. But that won't happen and there's is so much more useful
aspects to git.
.SH 2
Branching 
.PP 
Branching as it names implies allows you to create a diverging branch to your
git tree. Branching should become second nature as it is a good practice to
implement a feature on a separate branch without messing out your master branch.
Some create a branch for each versions, for each big functionality (or User
Story), or even a branch per platform (Linux, Windows) in some cases.
.br
In any case, to create a branch type (and choose a branch name) : 
.cm "git branch [BRANCH_NAME]"
Let's say we named our branch Sprint-1. The result can be mentally seen as : 
.\" fig2
.PS
C0 : circle "C0" at (0,0) shaded "lightblue"; 
box invisible "master" at (0,-0.5) outline "lightblue";
C1 : circle "C1" at (1,0) shaded "lightblue";
box invisible "master*" at (1,-0.5) outline "lightblue";
box invisible "Sprint-1" at (1,-0.7) outline "lightblue";
line from C0.e to C1.w;
HEAD : box invisible "HEAD*" at (1.5, 0.7);
arrow from HEAD.s to C1.n;
.PE
The new branch is up to date from the branch it was created from. 
.SH 2 
Checking out
.PP
Now is a good time to talk about "git checkout". This command allows you to move
around the tree. You can switch branches or go to previous commits
.br
.UL "switch branch : "
.cm "git checkout Sprint-1"
.UL "Go to commit C0 :"
.cm "git checkout C0"
result : 
.\" fig3
.PS
C0 : circle "C0" at (0,0) shaded "lightblue"; 
box invisible "master" at (0,-0.5) outline "lightblue";
C1 : circle "C1" at (1,0) shaded "lightblue";
box invisible "master*" at (1,-0.5) outline "lightblue";
box invisible "Sprint-1" at (1,-0.7) outline "lightblue";
line from C0.e to C1.w;
HEAD : box invisible "HEAD*" at (0.5, 0.7);
arrow from HEAD.s to C0.n;
.PE
.\" fig4
Now let's say you are on the commit C1 on the branch Sprint-1 and you decide to
commit.
The result will look like this : 
.PS
C0 : circle "C0" at (0,0) shaded "lightblue"; 
box invisible "master" at (0,-0.5) outline "lightblue";
C1 : circle "C1" at (1,0) shaded "lightblue";
box invisible "master*" at (1,-0.5) outline "lightblue";
C2 : circle "C2" at (2,0) shaded "red";
box invisible "Sprint-1" at (2,-0.5) outline "lightblue";
line 0.5 from C1.e to C2.w;
line from C0.e to C1.w;
HEAD : box invisible "HEAD*" at (2.5, 0.7);
arrow from HEAD.s to C2.n;
.PE
.\" fig5
.PS
C0 : circle "C0" at (0,0) shaded "lightblue"; 
box invisible "master" at (0,-0.5) outline "lightblue";
C1 : circle "C1" at (1,0) shaded "lightblue";
box invisible "master*" at (2,-0.5) outline "lightblue";
C2 : circle "C2" at (2,-1) shaded "red";
box invisible "Sprint-1" at (2,-1.5) outline "lightblue";
C3 : circle "C3" at (2,0) shaded "lightblue";
line from C1.e to C3.w;
box invisible "Sprint-1" at (2,-1.5) outline "lightblue";
spline down 0.5 from C1.s to C2.w;
line from C0.e to C1.w;
HEAD : box invisible "HEAD*" at (2.5, 0.7);
arrow from HEAD.s to C3.n;
.PE
Once you are happy with what you've done on the Sprint-1 branch and want to
integrate it to your master branch, you will wan't to do a "merge".
.SH 1
Merging
.PP
Merging allows you to integrate work from a branch to another. Merging makes a
new commit. To merge your Sprint-1 branch to master, you first need to be on the
branch "master" and type : 
.cm "git merge Sprint-1"
If all goes well the result should look like this. 
.PS
C0 : circle "C0" at (0,0) shaded "lightblue"; 
box invisible "master" at (0,-0.5) outline "lightblue";
C1 : circle "C1" at (1,0) shaded "lightblue";
box invisible "master*" at (2,-0.5) outline "lightblue";
C2 : circle "C2" at (2,-1) shaded "red";
box invisible "Sprint-1" at (2,-1.5) outline "lightblue";
C3 : circle "C3" at (2,0) shaded "lightblue";
line from C1.e to C3.w;
box invisible "Sprint-1" at (2,-1.5) outline "lightblue";
spline down 0.5 from C1.s to C2.w;
line from C0.e to C1.w;
C4 : circle "C4" at (3,0) shaded "lightblue";
spline right 0.5 from C2.e to C4.s;
line from C3.e to C4.w;
HEAD : box invisible "HEAD*" at (3.5, 0.7);
arrow from HEAD.s to C4.n;
.PE
.SH 2
Solving conflicts
.LP
But, everything doesn't always go as planned and sometimes git is not smart
enough to know what should be kept and what needs to be dropped. These are
called conflicts, and can require you to solve them manually. To do this there
are several different tools at your disposal, such vimdiff(1, 2, or 3),
gvimdiff, bc, etc. I've only tried vimdiff1, vimdiff2, and vimdiff3, which uses
vim, so I can't really talk about other tools.  
.code
void hello_world() {
<<<<<<< HEAD
printf("Hello world\n");
=======
char * name = "Meedos";
printf("Top o' the morning to you {}", name);
>>>>>>> C2
}
.LP
Here we have an example of two different implementations of a hello_world
function in C. One comes from the current branch we're on (master), and the
other implementation comes the branch we're trying to merge from (Sprint-1).
The part of code between the <<<<<<< and ======= markers is the code represents
the part of code present on the current working directory, in this case the
master branch. The code between the ======= and the >>>>>>> markers is the code
that comes from the branch we are merging from (here Sprint-1).\br
To solve this conflict you must choose whether you want to keep your
implementation, or the implementation of the other branch, or a bit both in some
cases. No matter what you choose to pick, you must remove all conflict markers
from the file. Once you are done solving all conflicts in you project, you must
commit your changes.\br
Vimdiff in all of this checks for all files that need manual merging, and cycles
through them in a terminal user interface until you are finished. 
.SH 1 
\ .gitignore
.PP
Often times you will have files in your repository you don't want to add to the
index or push it to the remote. These files can be logs, environment files,
files containing passwords, high volume files etc... It is possible to ignore
these files by specifying them in a ".gitignore" file. You can specify files,
directories, and also use wildcards to discard multiple targets.
.EX
.eg "log"
.eg "passwd"
.eg "images/*.png"
.LP
If log is a directory, the whole directory and its content will be ignored and
won't be added next time you enter a git add command. Same goes for the
passwd file and all files ending with .png in the images directory.\br
It is possible to check if a file or directory is correctly being ignored by git
with the following command : 
.cm "git check-ignore passwd"
If the path is outputted to stdout then the target is correctly ignored. If not
then something went wrong.

.SH 1
Rebasing
.PP
Rebasing is extremely important and very useful if you often have to rewrite
your commits. It is also useful when you work with a team, since you might have
to rebase your local branch from the main remote branch. We will look at those
two scenarios.
.SH 2
Rewritting history
.PP
Given the following rust code : 

.code
#[derive(Debug)]
struct Person {
    first_name: String,
    last_name: String,
    postal_adddress: String,
    age: u8,
    ss_number: u32,
}

fn main() {

    let person: Person = Person {
		first_name: String::from("John"),
		last_name:String::from("Doe"),
		postal_adddress: String::from("95 Passage de Vaugirard"),
		age: 42,
		ss_number: 012345678};

    println!("Hello {:?}", person);
    set_ss_number(person, 364798543);
}

fn set_ss_number(mut person: Person, new_ss: u32) {
    person.ss_number = new_ss;
}

.PP
Let's say you've made a mistake in one of your previous commits (not your last
one) and you would like to rewrite that commit to keep a clean history for your
colleagues.

.PSPIC -L images/git-graph-1.eps 3

We wish to rewrite the commit where we add the social security number attribute
instead of adding a fix commit on top of our history.

To do this type :
.cm "git rebase HEAD~2 -i"

.LP
You will then be prompted with a screen showing your two last commits, aswell as
commented help text below. 
The command we're interested in is the edit command which let's us modify our
commit. 

You should have the following:
.br
\m[blue]pick\m[] 535ff58 \m[darkgreen]add: function to set person's social security number\m[]
.br
\m[blue]pick\m[] 9965250 \m[darkgreen]add: person's postal address\m[]

From which we want to change to :
.br
\m[blue]edit\m[] 535ff58 \m[darkgreen]add: function to set person's social security number\m[]
.br
\m[blue]pick\m[] 9965250 \m[darkgreen]add: person's postal address\m[]
.br
Then save and exit
.br
.code
Proceed to make the following changes : 
diff --git a/src/main.rs b/src/main.rs
index d5a9567..497b9b5 100644
--- a/src/main.rs
+++ b/src/main.rs
@@ -3,7 +3,7 @@ struct Person {
     first_name: String,
     last_name: String,
     age: u8,
-    ss_number: u32,
+    ss_number: String,
 }

 fn main() {
@@ -11,7 +11,7 @@ fn main() {
     let person: Person = Person {first_name: String::from("John"),
	 last_name:String::from("Doe"),
     age: 42, ss_number: 012345678};
     println!("Hello {:?}", person);
-    set_ss_number(person, 364798543);
+    set_ss_number(person, String::from("364798543"));
 }

.LP
 Save your file and make your commit:
.cm "git commit --amend --no-edit"
And finally:
.cm "git rebase --continue"
You're done, you've successfully rewritten history.
